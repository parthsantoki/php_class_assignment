		$(document).ready(function(){

				
		// first name 
			$(".fname").blur(function(){
				var fname = $(".fname").val();
		    	if(fname == "")
		    	{
		    		$("#error_fname").addClass("opacity");
		    		$("#error_fname").text("Field required");
		    	}
		    	else
		    	{
		    		$("#error_fname").removeClass("opacity");
		    	}
			});

		// LAST NAME
			$(".lname").blur(function(){
				var lname = $(".lname").val();
		    	if(lname == "")
		    	{
		    		$("#error_lname").addClass("opacity");
		    		$("#error_lname").text("Field required");
		    	}
		    	else
		    	{
		    		$("#error_lname").removeClass("opacity");
		    	}
			});


		// // GENDER
		// 	$(".gender").blur(function(){
		// 		var gender = $(".gender:checked").val();
		//     	if(gender == "")
		//     	{
		//     		$("#error_gender").addClass("opacity");
		//     		$("#error_gender").text("Field required");
		//     	}
		//     	else
		//     	{
		//     		$("#error_gender").removeClass("opacity");
		//     	}
		// 	});



		/*
		// HOBBY 
	
			$(".hobby").blur(function(){
				var hobby = [];
            	$.each($("input[name='hobby']:checked"), function(){
                		hobby.push($(this).val());
            		});
		    	if(hobby == "")
		    	{
		    		$("#error_hobby").addClass("opacity");
		    		$("#error_hobby").text("Field required");
		    	}
		    	else
		    	{
		    		$("#error_hobby").removeClass("opacity");
		    	}
			});
					*/

		// NUMBER 
			$(".number").blur(function(){
				var number = $(".number").val();
		    	if(number == "")
		    	{
		    		$("#error_number").addClass("opacity");
		    		$("#error_number").text("Field required");
		    	}
		    	else
		    	{

		    	if(number.length != 10)
		    	{
		    		$("#error_number").addClass("opacity");
		    		 $("#error_number").text("enter 10 number");
		    	}
		    	else
		    	{
		    		$("#error_number").removeClass("opacity");
		    	}
		    	}
		    });


		//EMAIL
			$(".email").blur(function(){
				var email = $(".email").val();
				var emial_split = email.split(".").reverse();
				var emial_split_length = emial_split[0].length;
		    	
				if(email == "")
				{
					$("#error_email").addClass("opacity");
		    		$("#error_email").text("Field is required");
				}
				else
				{
		    	if(emial_split_length > 3)  // for max length 3 after . in email
				{
					$("#error_email").addClass("opacity");
		    		$("#error_email").text("Enter valid emails");
				}
				else
		    	{
		    		$("#error_email").removeClass("opacity");
		    	}
		   		}
		    });

		//PASSWORD
			$(".password").blur(function(){
				var password = $(".password").val();
				var password_array = Array.from(password);
				var repassword = $(".repassword").val();

				if(password == "")
				{
					$("#error_password").addClass("opacity");
					$("#error_password").text("Field is required");
				}
				else
				{
		    	if(password.length >= 8)  // for max length 3 after . in email
				{	
					$("#error_password").removeClass("opacity");
					if(password.match(/[A-Z]/))
					{	
						$("#error_password").removeClass("opacity");
						if(password.match(/[!@#$%^&*]/))
						{	
							$("#error_password").removeClass("opacity");
							if(password.match(/[0-9]/))
							{	
								$("#error_password").removeClass("opacity");
								if(password != repassword)
								{
									$("#error_repassword").addClass("opacity");
									$("#error_repassword").text("Password not match");
								}
								else
								{
									$("#error_repassword").removeClass("opacity");
								}
							}
							else
							{
								$("#error_password").addClass("opacity");
								$("#error_password").text("Number Must be there");
							}
						}
						else
						{
							$("#error_password").addClass("opacity");
							$("#error_password").text("Special Must be there");
						}
					}
					else
					{
						$("#error_password").addClass("opacity");
						$("#error_password").text("Capital Must be there");
					}
				}
				else
				{
					$("#error_password").addClass("opacity");
		    		$("#error_password").text("Minimum 8 char");
				}
				}
		    });


		//REPASSWORD
		    $(".repassword").blur(function(){
				var password = $(".password").val();
				var repassword = $(".repassword").val();
				if(password != repassword)
				{
					$("#error_repassword").addClass("opacity");
					$("#error_repassword").text("Password not match");
				}
				else
				{
					$("#error_repassword").removeClass("opacity");
				}
			});


		// SUBMIT 
		    $("#submit").click(function(){

		    	var fname = $(".fname").val();
				var lname = $(".lname").val();
		    	var number = $(".number").val();
		    	var email = $(".email").val();
				var password = $(".password").val();
				var repassword = $(".repassword").val();
				var stream = $(".stream").val();
				var address = $(".address").val();
				var gender = $(".gender:checked").val();
				var hobby = [];
            	$.each($("input[name='hobby']:checked"), function(){
                		hobby.push($(this).val());
            		});

		    	var error_fname = $("#error_fname").attr("class");
		    	var error_lname = $("#error_lname").attr("class");
		    	var error_email = $("#error_email").attr("class");
		    	var error_password = $("#error_password").attr("class");
		    	var error_repassword = $("#error_repassword").attr("class");
		    	var error_number = $("#error_number").attr("class");

			 if ( error_fname != "opacity" &&  error_lname != "opacity" &&  error_email != "opacity" &&  error_password != "opacity" &&  error_number != "opacity" &&  error_repassword != "opacity")
			 {
			 	if(fname != "" &&  lname != "" &&  email != "" &&  password != "" &&  number != "")
			 	{
			 		//alert("First name :- "+fname+"\nLast name :- "+lname+"\nMobile number :- "+number+"\nEmail :- "+email+"\nStream :- "+stream+"\nAddress :- "+address+"\nHobby :- "+hobby+"\nGender :- "+gender+"\nPassword :- "+password);
			 	}
			 	else
			 	{
			 		alert("please enter value in every field");		
			 	}
			 }
			 else
			 {
			 	alert("please enter value as mentioned");
			 }
			});
		  });
