<?php 
session_start();
$userId = $_SESSION['userId'];
$addUser = new addUser($userId);

class addUser
{	
	function __construct($userId)
	{
		
		//INCLUDE AND CREAT OBJECT OF MODEL
		include '../model/index.php';
		$model = new model();
		
		//ESABLISHING CONNECTION 
		$conn = $model -> connection();

		//INCLUDING DASHBOARD PAGE
		include '../view/add_user.php';

		if($_SERVER["REQUEST_METHOD"] == "POST")
		{	
			if(isset($_POST['submit']))
			{
				$firstName = isset($_POST['firstName']) ? (string)$_POST['firstName'] : "";
				$lastName = isset($_POST['lastName']) ? (string)$_POST['lastName'] : "";
				$mobileNumber = isset($_POST['mobileNumber']) ? (int)$_POST['mobileNumber'] : "";
				$email = isset($_POST['email']) ? (string)$_POST['email'] : "";
				$gender = isset($_POST['gender']) ? $_POST['gender'] : "";
				$hobby = isset($_POST['hobby']) ? (string)$_POST['hobby'] : "";
				$password = isset($_POST['password']) ? (string)$_POST['password'] : "";


				//INSERTING DATA
				$data = $model->  addUser($firstName,$lastName,$gender,$hobby,$mobileNumber,$email,$password,$conn);

				if(isset($data) && $data == 1)
				{
					header("location: dashboard.php");
				}
			}
		}
		
	}
}
?>