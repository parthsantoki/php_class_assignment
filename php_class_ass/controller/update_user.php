<?php 
session_start();

$userId = isset($_GET['userId']) ? (int)$_GET['userId'] : "";
$_SESSION['userId'] = $userId;
$updateUser = new updateUser($userId);

class updateUser
{	
	function __construct($userId)
	{
		
		//INCLUDE AND CREAT OBJECT OF MODEL
		include '../model/index.php';
		$model = new model();
		
		//ESABLISHING CONNECTION 
		$conn = $model -> connection();

		$data = $model->  selectData($conn,$userId);


		//INCLUDING updateUser PAGE
		include '../view/update_user.php';	
		
	}
}

?>