<?php 
session_start();
$loginUserId = $_SESSION['loginUserId'];

$dashboard = new dashboard($loginUserId);

class dashboard
{	
	function __construct($userId)
	{
		
		//INCLUDE AND CREAT OBJECT OF MODEL
		include '../model/index.php';
		$model = new model();
		
		//ESABLISHING CONNECTION 
		$conn = $model -> connection();

		$data = $model->  dashboard($conn);
		//INCLUDING DASHBOARD PAGE
		include '../view/dashboard.php';
	}
}

?>