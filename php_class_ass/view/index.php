<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Add User</title>
	<link rel="stylesheet" href="assets/css/style.css">
	<script src="assets/js/jquery-3.4.1.min.js" ></script>
</head>
<body>	
	<div class="reg-form">
		<center>
		<form  method="post" accept-charset="utf-8" name="regform">
			<h1>Login Form</h1>
			
			<!-- USER NAME -->	
			<input type="email" class="email" name="userName" placeholder="User name" required>

			<!-- PASSWORD -->
			<input type="password" class="password" name="password" placeholder="Password" required>

			<!-- SUBMIT BUTTON -->
			<input type="submit" value="Submit" name="submit" class="submit" id="submit">

			<!-- RESET BUTTON -->
			<input type="reset" value="Reset" class="submit">
		</form>
		</center>
	</div>
	
</body>
<!-- <script src="assets/js/js_validation.js" type="text/javascript" charset="utf-8" async defer></script> -->
</html>