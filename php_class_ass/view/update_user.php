<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Update User</title>
	<link rel="stylesheet" href="../assets/css/style.css">
	<script src="../assets/js/jquery-3.4.1.min.js" ></script>
</head>
<body>	
	<div class="reg-form">
		<center>
		<form action="update_data.php" method="post" accept-charset="utf-8" name="regform">
			<h1>Update user</h1>
			<!-- FIRST NAME -->	
			<input type="text" class="fname" placeholder="First name" name="firstName" value="<?php echo $data['first_name']; ?>" >
			<p id="error_fname">Error</p> 

			<!-- LAST NAME -->
			<input type="text" class="lname" placeholder="Last name" name="lastName" value="<?php echo $data['last_name']; ?>" >
			<p id="error_lname">Error</p>

			<!-- MOBILE NUMBER -->
			<input type="number" class="number" placeholder="Mobile number" name="mobileNumber" maxlength="10" value="<?php echo $data['mobile_number']; ?>">
			<p id="error_number">Error</p>

			<!-- EMAIL -->
			<input type="email" class="email" placeholder="Email" name="email" value="<?php echo $data['email']; ?>">
			<p id="error_email">Error</p>

			<!-- GENDER -->
			<label class="gender_label" >Gender</label>
			<input type="radio" name="gender" class="gender" value="male" <?php echo ($data['gender'] == 'male' ? 'checked':'')?> >Male
			<input type="radio" name="gender" class="gender" value="female" <?php echo ($data['gender'] == 'female' ? 'checked':'')?>>Female <br> <br>
			<p id="error_gender">Error</p>

			<!-- HOBBY -->
			<label>Hobby</label>
			<input type="checkbox" name="hobby" class="hobby" value="cricket">cricket
			<input type="checkbox" name="hobby" class="hobby" value="football">football
			<input type="checkbox" name="hobby" class="hobby" value="dance">dance <br><br>
			<p id="error_hobby">Error</p>

			<!-- IMAGE UPLOAD -->
			<input type="file" name="imageUpload" value="upload image" accept="image/png, image/jpeg, image/jpg">
			<p id="error_image_upload">Error</p>

			<!-- PASSWORD -->
			<input type="password" class="password" name="password" placeholder="Password" value="<?php echo $data['password']; ?>" >
			<p id="error_password">Error</p>

			<!-- CONFIRM PASSWORD -->
			<input type="password" class="repassword" placeholder="Confirm Password" value="<?php echo $data['password']; ?>">
			<p id="error_repassword">Error</p>

			<!-- SUBMIT BUTTON -->
			<input type="submit" value="Submit" name="submit" class="submit" id="submit">

		</form>
		</center>
	</div>

	
</body>
<!-- <script src="../assets/js/js_validation.js" type="text/javascript" charset="utf-8" async defer></script> -->
</html>