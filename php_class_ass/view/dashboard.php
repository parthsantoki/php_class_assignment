<?php 

?>

<!DOCTYPE html>
<html>
<head>
	<title>DASHBOARD</title>
	<style type="text/css">
		th,td,table
		{
			border: 1px solid black;
			border-collapse: collapse;
			padding: 10px;
		}
	</style>
</head>
<body>
<table>
	<h1>DASHBOARD</h1>
	<button><a href="add_user.php">Add user</a></button>
	
	<tr>
		<th>Fisrt name</th>
		<th>Last name</th>
		<th>Gender</th>
		<th>Hobby</th>
		<th>Mobile Number</th>
		<th>Email</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
		<?php 
			foreach ($data as $i)
			{
				echo "<tr>";
				echo "<td>".$i['first_name']."</td>";
				echo "<td>".$i['last_name']."</td>";
				echo "<td>".$i['gender']."</td>";
				echo "<td>".$i['hobby']."</td>";
				echo "<td>".$i['mobile_number']."</td>";
				echo "<td>".$i['email']."</td>";
				echo "<td><button><a href='update_user.php?userId=".$i['id']."'>update</a></button></td>";
				echo "<td><button><a href='delete_user.php?userId=".$i['id']."'>delete</a></button></td>";
				echo "</tr>";
			}
		?>
		
</table>
</body>
</html>